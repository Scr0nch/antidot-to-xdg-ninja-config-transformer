mod file_actions;
mod help_text;

use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::Path;

use clap::Parser;
use clap_verbosity_flag::WarnLevel;
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

/// The number of spaces per indent in the output xdg-ninja JSON configuration files
const JSON_INDENT_SPACE_COUNT: usize = 4;
const JSON_INDENT_STRING: &[u8] = &[b" "[0]; JSON_INDENT_SPACE_COUNT];

const GENERATED_CONFIGURATION_FILES_DIRECTORY_NAME: &str = "generated-files";

/// Program to transform a antidot configuration file to xdg-ninja configuration files
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct CommandLineArguments {
    #[clap(flatten)]
    verbose: clap_verbosity_flag::Verbosity<WarnLevel>,

    /// The path to the antidot rules file to parse
    #[clap(short, long, default_value = "rules.yaml")]
    antidot_rules_path: String,

    /// The path to the directory containing the existing xdg-ninja configuration files. Generated
    /// configuration files will be placed inside this directory in a directory named
    /// GENERATED_CONFIGURATION_FILES_DIRECTORY_NAME.
    #[clap(short, long, default_value = "")]
    xdg_ninja_configuration_files_path: String,
}

#[derive(Debug, Deserialize)]
struct AntidotConfig {
    // version: u32,
    rules: Vec<Rule>,
}

#[derive(Debug, Deserialize)]
pub struct Rule {
    name: String,
    //description: Option<String>,
    notes: Option<Vec<String>>,
    dotfile: Dotfile,
    actions: Vec<Action>,
}

#[derive(Debug, Deserialize)]
struct Dotfile {
    // #[serde(rename = "is_dir")]
    // is_directory: Option<bool>,

    name: String,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(tag = "type")]
pub enum Action {
    #[serde(rename = "migrate")]
    Migrate {
        source: String,

        #[serde(rename = "dest")]
        destination: String,
    },
    #[serde(rename = "delete")]
    Delete {
        // path: String
    },
    #[serde(rename = "export")]
    Export {
        key: String,
        value: String,
    },
    #[serde(rename = "alias")]
    Alias {
        alias: String,
        command: String,
    },
}

#[derive(Debug, Serialize)]
struct XdgNinjaProgramConfig {
    name: String,
    files: Vec<XdgNinjaFileConfig>,
}

#[derive(Debug, Serialize)]
struct XdgNinjaFileConfig {
    path: String,
    moveable: bool,
    help: String,
}

fn main() {
    let args: CommandLineArguments = CommandLineArguments::parse();

    // Initialize logger from verbosity command line argument
    env_logger::Builder::new()
        .filter_level(args.verbose.log_level_filter())
        .init();

    let antidot_rules_path = Path::new(&args.antidot_rules_path);
    let xdg_ninja_configuration_files_path = Path::new(&args.xdg_ninja_configuration_files_path);

    // Ensure the xdg-ninja generated configuration files directory exists
    let create_xdg_ninja_rules_directory = create_dir_all(xdg_ninja_configuration_files_path.join(GENERATED_CONFIGURATION_FILES_DIRECTORY_NAME));
    if create_xdg_ninja_rules_directory.is_err() {
        error!("Error occurred while creating the xdg-ninja rules directory: {}", create_xdg_ninja_rules_directory.err().unwrap());
        std::process::exit(-1);
    }

    info!("Reading antidot configuration file at \"{}\"...", args.antidot_rules_path);

    let file = File::open(antidot_rules_path);
    if file.is_err() {
        error!("Error occurred while reading antidot rules file: {}", file.err().unwrap());
        std::process::exit(-1);
    }

    let file = file.unwrap();
    debug!("Read antidot configuration file");

    info!("Parsing antidot configuration file...");

    let antidot_configuration = serde_yaml::from_reader::<File, AntidotConfig>(file);
    if antidot_configuration.is_err() {
        error!("Error occurred while parsing antidot rules file: {}", antidot_configuration.err().unwrap());
        std::process::exit(-1);
    }

    let antidot_configuration = antidot_configuration.unwrap();
    debug!("Parsed antidot configuration file");
    trace!("{:#?}", antidot_configuration);

    let mut generated_program_configurations: Vec<XdgNinjaProgramConfig> = vec!();

    info!("Generating program configurations...");

    // Keep track of the number of skipped rules to output as an informational message later
    let mut skipped_rules_count: u32 = 0;

    for rule in antidot_configuration.rules {
        if xdg_ninja_configuration_files_path.join(Path::new(&rule.name).with_extension("json")).exists() {
            trace!("Skipping rule \"{}\" since it already exists", &rule.name);
            skipped_rules_count += 1;
            continue;
        }

        // Construct a map between destination filenames and a list of actions relevant to that
        // file or directory so that the actions for a single file or directory can be combined
        // into a single help message
        let file_actions = file_actions::create_file_actions(&rule);

        trace!("File actions: {:#?}", file_actions);

        let mut file_configurations: Vec<XdgNinjaFileConfig> = vec![];

        // Generate a xdg-ninja file configuration for each file entry in the map
        for (destination_file_path, file_actions) in file_actions {
            let help_text = help_text::create_help_text(&file_actions);
            let source_file_path = get_source_file_path(&rule, &file_actions);
            if source_file_path.is_none() {
                error!("Destination file path \"{}\" does not have an associated migrate action and the dotfile path for its rule ({:#?}) is not valid UTF-8!", destination_file_path, rule);
                std::process::exit(-1);
            }

            file_configurations.push(XdgNinjaFileConfig {
                path: source_file_path.unwrap(),
                moveable: !help_text.is_empty(),
                help: help_text,
            });
        }

        generated_program_configurations.push(XdgNinjaProgramConfig {
            name: rule.name.to_owned(),
            files: file_configurations,
        });

        // Warn the user about information automatically discarded during the conversion process
        if let Some(notes) = &rule.notes {
            warn!("Notes for rule \"{}\" should be considered manually:\n{:#?}", &rule.name, notes);
        }
    }

    info!("Skipped {} rules due to already-existing xdg-ninja configurations", skipped_rules_count);
    debug!("Generated {} program configurations from antidot configuration", generated_program_configurations.len());
    trace!("Generated rules: {:#?}", generated_program_configurations);

    for program_configuration in &generated_program_configurations {
        // Create custom formatter to write JSON in a pretty format with a customizable amount of
        // indent spaces
        let formatter = serde_json::ser::PrettyFormatter::with_indent(JSON_INDENT_STRING);
        let serializer_buffer = Vec::new();
        let mut pretty_serializer = serde_json::Serializer::with_formatter(serializer_buffer, formatter);

        let serialize_result = program_configuration.serialize(&mut pretty_serializer);
        if serialize_result.is_err() {
            error!("Error occurred while serializing program configuration: {:#?}", program_configuration);
            std::process::exit(-1);
        }

        let json_string = String::from_utf8(pretty_serializer.into_inner());
        if json_string.is_err() {
            error!("Error occurred while serializing program configuration ({:#?}): {}", program_configuration, json_string.err().unwrap());
            std::process::exit(-1);
        }

        let program_configuration_file_path = xdg_ninja_configuration_files_path
            .join(GENERATED_CONFIGURATION_FILES_DIRECTORY_NAME)
            .join(&program_configuration.name)
            .with_extension("json");
        let program_configuration_file = File::create(&program_configuration_file_path);
        if program_configuration_file.is_err() {
            error!("Error occurred while creating program configuration file at path {:#?}: {}", &program_configuration_file_path, program_configuration_file.err().unwrap());
            std::process::exit(-1);
        }
        let mut program_configuration_file = program_configuration_file.unwrap();

        let write_result = writeln!(&mut program_configuration_file, "{}", json_string.unwrap());
        if write_result.is_err() {
            error!("Error occurred while writing serialized json for program configuration to file \"{:#?}\": {}", &program_configuration_file, write_result.err().unwrap());
            std::process::exit(-1);
        }

        trace!("Wrote configuration file to {}", program_configuration_file_path.display());
    }

    info!("Wrote {} configurations to xdg-ninja configuration directory \"{}\"", generated_program_configurations.len(), xdg_ninja_configuration_files_path.display());
}

fn get_source_file_path(rule: &Rule, file_actions: &Vec<Action>) -> Option<String> {
    for file_action in file_actions {
        if let Action::Migrate { source, destination: _destination } = file_action {
            return Some(source.to_owned());
        }
    }

    // Fall back to the path of the dotfile if no source file path is available
    let dotfile_path = Path::new(&rule.dotfile.name).to_str();
    if let Some(dotfile_directory_path) = dotfile_path {
        return Some(dotfile_directory_path.to_string());
    }

    return None;
}