use std::collections::HashMap;
use log::warn;
use crate::{Action, Rule};

/// Creates a map of destination file paths to actions for the actions in the given `rule`.
///
/// `Delete` actions are ignored since no mention of deleting files was found in the current version
/// of xdg-ninja.
pub fn create_file_actions(rule: &Rule) -> HashMap<String, Vec<Action>> {
    let mut file_actions: HashMap<String, Vec<Action>> = HashMap::new();

    for action in &rule.actions {
        match &action {
            Action::Migrate { source: _, destination } => {
                file_actions.entry(destination.to_owned()).or_insert(vec![]).push(action.clone());
            }
            Action::Delete {} => {
                // Delete actions are not mentioned in xdg-ninja, so they are safe to ignore here
            }
            Action::Export { key: _, value } => {
                insert_export_or_alias_action_into_file_actions(&mut file_actions, value, &action);
            }
            Action::Alias { alias: _, command } => {
                insert_export_or_alias_action_into_file_actions(&mut file_actions, command, &action);
            }
        }
    }

    return file_actions;
}

/// Finds the destination path in the given string for the given action and inserts the given action
/// into the given `file_actions` map with the correct destination path key.
fn insert_export_or_alias_action_into_file_actions(file_actions: &mut HashMap<String, Vec<Action>>, string_containing_file_path: &String, action: &Action) {
    let xdg_path = find_xdg_path_in_string(string_containing_file_path);
    if xdg_path.is_none() {
        warn!("Could not find path inside action (\"{:#?}\"), skipping!", action);
        return;
    }
    let xdg_path = xdg_path.unwrap();

    let mut found_entry = false;
    for entry in file_actions.iter_mut() {
        if entry.0.contains(&xdg_path) {
            entry.1.push(action.clone());
            found_entry = true;
            break;
        }
    }

    if !found_entry {
        file_actions.insert(xdg_path, vec![action.clone()]);
    }
}

/// Finds and returns a path beginning with XDG_ inside the given shell `string` if one exists.
///
/// This function properly handles the shell syntax seen in the current version of antidot's rules
/// file, including semicolon line termination, curly bracing, and quotations.
fn find_xdg_path_in_string(string: &String) -> Option<String> {
    // Remove quotations
    let unquoted_string = string.to_owned().replace("\"", "");

    // All paths in export actions' values and alias actions' commands start with ${XDG_... or
    // $XDG... find the first occurrence of this in the string
    let braced_xdg_path = find_specific_xdg_path_in_string(&unquoted_string, "${XDG_");
    if braced_xdg_path.is_some() {
        return braced_xdg_path;
    }
    let unbraced_xdg_path = find_specific_xdg_path_in_string(&unquoted_string, "$XDG_");
    if unbraced_xdg_path.is_none() {
        return None;
    }

    // Add curly braces around the XDG_... substring for uniformity
    let mut unbraced_xdg_path = unbraced_xdg_path.unwrap();
    unbraced_xdg_path.insert(1, '{');
    unbraced_xdg_path.insert(unbraced_xdg_path.find('/')?, '}');

    return Some(unbraced_xdg_path);
}

/// Finds and returns a path beginning with `xdg_string` inside the given shell `string` if one
/// exists.
fn find_specific_xdg_path_in_string(string: &String, xdg_string: &str) -> Option<String> {
    let path_start_index = string.find(xdg_string);
    if path_start_index.is_none() {
        return None;
    }

    let path_start_index = path_start_index.unwrap();
    // Find the end of the path, which is either when a whitespace or semicolon character is
    // encountered, offset by the start index. If neither of these conditions are met, the path ends
    // at the end of the string.
    let mut path_stop_index = string[path_start_index..].find(|c: char| c.is_whitespace() || c == ';')
        .and_then(|path_stop_index| Some(path_stop_index + path_start_index))
        .unwrap_or(string.len());

    // Remove any trailing slashes from the path for uniformity
    if string.chars().nth(path_stop_index - 1).unwrap() == '/' {
        path_stop_index -= 1;
    }

    return Some(string[path_start_index..path_stop_index].to_owned());
}