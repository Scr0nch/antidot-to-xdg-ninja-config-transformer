use crate::{Action};

/// Creates a Markdown help text string for a given list of `Action`s.
///
/// `Export` and `Alias` actions are prioritized such that is one exists, `Migrate` actions are
/// ignored as seen in the current version of xdg-ninja's configurations.
pub fn create_help_text(file_actions: &Vec<Action>) -> String {
    let mut help_text = String::new();

    let mut has_export_action = false;
    for file_action in file_actions {
        if let Action::Export { key, value } = file_action {
            if !has_export_action {
                help_text.push_str("Export the following environment variables:\n\n```bash\n");
            }
            help_text.push_str(&*format!("export {}={}\n", key, value));
            has_export_action = true;
        }
    }
    if has_export_action {
        help_text.push_str("```\n");
    }

    let mut has_alias_action = false;
    for file_action in file_actions {
        if let Action::Alias { alias, command } = file_action {
            if !has_alias_action {
                help_text.push_str(&*format!("Alias {} to use custom locations:\n\n```bash\n", alias));
            }
            help_text.push_str(&*format!("alias {}={}", alias, command));
            has_alias_action = true;
        }
    }
    if has_alias_action {
        help_text.push_str("```\n");
    }

    if !has_export_action && !has_alias_action {
        let mut has_migrate_action = false;
        for file_action in file_actions {
            if let Action::Migrate { source, destination } = file_action {
                if !has_migrate_action {
                    help_text.push_str(&*format!("Supported\n"));
                }
                help_text.push_str(&*format!("\nThe file {} can be moved to {}.\n", source, destination));
                has_migrate_action = true;
            }
        }
    }

    return help_text;
}