# antidot-to-xdg-ninja-config-transformer

A command-line program written in Rust to transform an [`antidot`](https://github.com/doron-cohen/antidot) rules file to
[`xdg-ninja`](https://github.com/b3nj5m1n/xdg-ninja) program configuration files.

This is my first Rust project, so expect some un-ironic programming horror.

## Usage

```shell
cargo run -- -a path/to/rules.yaml -x path/to/programs/directory/
```

Generated program configurations will be placed in `path/to/programs/directory/generated-files/`.

If a program configuration already exists in `path/to/programs/directory/`, one will not be generated.

By default, the log level is set to warning so that messages notifying the user when manual intervention may be needed
are shown. Manual review of all generated files is recommended for personalization.